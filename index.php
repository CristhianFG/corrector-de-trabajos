<?php
	#FUNCION: Pagina principal de la aplicación.
	#AUTOR: Cristhian Ferreiro Garrido
	#FECHA: Ultima edición 26/05/2018
?>
<html>
	<head><meta charset="utf-8">
		<style>
			body {
				font-family: Verdana;
				color: #000000;
				background-color: #3BDFCD }
				
			h2 {
				font-family: Courier; }
				
			.lista {
				width: 75%; 
				height: auto!important;
				background-image: url("http://www.servinet.net/wp-content/uploads/2016/09/fondo-web-blanco.jpg"); 
				border-radius: 25px;
				border: 3px solid #000;	}
				
				
			input[id^="spoiler"]{
				display: none;
			}
			input[id^="spoiler"] + label {
				display: block;
				width: 200px;
				margin: 0 auto;
				padding: 5px 20px;
				background: #e1a;
				color: #fff;
				text-align: center;
				font-size: 24px;
				border-radius: 8px;
				cursor: pointer;
				transition: all .6s;
			}
			input[id^="spoiler"]:checked + label {
				color: #333;
				background: #ccc;
			}
			input[id^="spoiler"] ~ .spoiler {
				width: 90%;
				height: 0;
				overflow: hidden;
				opacity: 0;
				margin: 10px auto 0; 
				padding: 10px; 
				background-image: url("fondo2.jpg");
				border: 1px solid #ccc;
				border-radius: 8px;
				transition: all .6s;
			}
			input[id^="spoiler"]:checked + label + .spoiler{
				height: auto;
				opacity: 1;
				padding: 10px;
			}
			
			.btn {
			  -webkit-border-radius: 60;
			  -moz-border-radius: 60;
			  border-radius: 60px;
			  font-family: Georgia;
			  color: #000000;
			  font-size: 20px;
			  background: #3dff7a;
			  padding: 10px 25px 10px 25px;
			  border: solid #000000 3px;
			  text-decoration: none;
}

		.btn:hover {
			  background: #3cb0fd;
			  background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
			  background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
			  background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
			  background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
			  background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
			  text-decoration: none;
}
		</style>
	</head>
	
	<body background="fondo1.jpg">
		
		<br><br><br><br><br><br><br>

		<center><div class="lista"><center><br><br><br><br>
		
		<?php
			echo "Asegurese de haber guardado el Codigo Fuente en 'CodigoAExaminar' y haber configurado correctamente los ficheros 'Directories.Conf' y 'Files.Conf' antes de continuar: ";
		?>
	
		<br><br><br>
	
		<div id="checkbox">
			<input type="submit" class="btn" aling="right" name="lanzar" value="EVALUAR" onClick=" window.location.href='./Controller.php?Evaluar' ">
		</div>
			
		<br><br><br><br></div><br><br><br><br><br><br></div></body>
</html>