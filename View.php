<?php
	#FUNCION: Vista de los resultados en detalle de la aplicación.
	#AUTOR: Cristhian Ferreiro Garrido
	#FECHA: Ultima edición 26/05/2018
?>

<html>
	<head><meta charset="utf-8">
		<style>
			body {
				font-family: Courier;
				color: #000000;
				background-color: #3BDFCD }
				
			h2 {
				font-family: Courier; }
				
			.lista {
				width: 75%; 
				height: auto!important;
				background-image: url("fondo2.jpg"); 
				border-radius: 25px;
				border: 3px solid #000;	}
		</style>
	</head>
	
	<body background="fondo1.jpg">
		
		<?php
		class Vista{
		
		#Crea la vista de los resultados.
		function crear($evaluacion){ ?>
		
		<br><br><br><br><br><br><br>

		<center><div class="lista">
<?php
		
		$i=0;
		#Recorre y muestra el contenido de evaluacion.
		while($i<count($evaluacion)){
			echo $evaluacion[$i];
			$i++;
		}
?>							
			<br><br><br><br><br><br></div></center>
		<?php }} ?>
			</body>
</html>