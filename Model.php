<?php
	#FUNCION: Modelo de la aplicación.
	#AUTOR: Cristhian Ferreiro Garrido
	#FECHA: Ultima edición 27/06/2018

class Modelo{
	
	#Devuelve el contenido de un fichero de configuración valida. 
	function configuracion($link){
		$contenido = explode("\n" , file_get_contents($link));
		for($i=0;$i<count($contenido);$i++){
			$contenido[$i]=trim($contenido[$i]);
		}
		return $contenido;
	}
	
	#Devuelve la lista de ficheros en la raiz.
	function listarFicherosInicio(){
		$ficheros = scandir('CodigoAExaminar');
		return $ficheros;
	}
	
	
	#Devuelve la lista de ficheros de una carpeta.
	function listarFicheros($direccion){
		$extensiones=array('php','phps','txt','doc','odt','js','html','htmls','bak','conf');
		$files=array();
		if(is_dir($direccion)){
			$ficheros = scandir($direccion);
		}
		else{
			$ficheros=array();
		}
		#Recorre la lista de ficheros de cada directorio.
		for($i=0;$i<count($ficheros);$i++){
			if($ficheros[$i]!='.' && $ficheros[$i]!='..'){
				$actual = $direccion.'/'.$ficheros[$i];
				#Comprueba los ficheros
				if(is_file($actual)){
					$info = pathinfo($actual);
					$exten = $info['extension'];
					$k=0;
					$add=FALSE;
					while($k<count($extensiones) && !$add){
						if($exten==$extensiones[$k]){
							$files[]=$actual;
							$add=true;
						}
						$k++;
					}
				}
				#Comprueba los directorios
				if(is_dir($actual)){
					$model = new Modelo();
					$hijo = $model->listarFicheros($actual);
					if($hijo!=0){
						for($j=0;$j<count($hijo);$j++){
							$files[]=$hijo[$j];
						}
					}
					unset($hijo);
				}
			}	
		}
		return $files;
	}
	

	
	#Devuelve el codigo de todos los ficheros de una carpeta concreta.
	function listarFicherosTipo($tipo){
		$files=array();
		$actualDir='CodigoAExaminar/'.$tipo;
		#Comprueba que sea un directorio.
		if(is_dir($actualDir)){
			$ficheros = scandir($actualDir);
		}
		else{
			$ficheros=array();
		}
		#Recorre la lista de ficheros de cada directorio.
		for($j=0;$j<count($ficheros);$j++){
			$actualFil = $actualDir.'/'.trim($ficheros[$j]);
			#Comprueba si es un fichero.
			if(is_file($actualFil)){
				$files[]=$actualFil;
			}
		}
		return $files;
	}
		
}
?>