<?php
	#FUNCION: Controlador de la aplicación.
	#AUTOR: Cristhian Ferreiro Garrido
	#FECHA: Ultima edición 27/06/2018

	include("View.php");
	include("Model.php");

	
	#Si viene del index ejecuta el siguiente codigo.
	if(isset($_REQUEST['Evaluar'])){
		$evaluacion=array();
		$model = new Modelo();
		$dirConf = $model->configuracion('./Directories.conf'); #Apartado 1
		$filesInicio = $model->listarFicherosInicio(); #Apartado 1
		$filConf = $model->configuracion('./Files.conf'); #Apartado 2
		$filesCodigo = $model->listarFicheros('CodigoAExaminar'); #Apartado 2,3,4,5,6
		$controllers = $model->listarFicherosTipo("Controller"); #Apartado 7
		$models = $model->listarFicherosTipo("Model"); #Apartado 8
		$views = $model->listarFicherosTipo("View"); #Apartado 9
			
		$evaluacion[0]='<br><h2>DETALLE:</h2><br>';
		$evaluacion[1]=comprobacion1($dirConf,$filesInicio);
		$evaluacion[2]=comprobacion2($filConf,$filesCodigo);
		$evaluacion[3]=comprobacion3($filesCodigo);
		$evaluacion[4]=comprobacion4($filesCodigo);
		$evaluacion[5]=comprobacion5($filesCodigo);
		$evaluacion[6]=comprobacion6($filesCodigo);
		$evaluacion[7]=comprobacion7($models);
		$evaluacion[8]=comprobacion8($controllers);
		$evaluacion[9]=comprobacion9($views);
			
		$vista = new Vista();
		$vista->crear($evaluacion);
	}
		
	#Realiza las comprobaciones del apartado 1.	
	function comprobacion1($dirConf,$filesInicio){
		$correctos=0;
		$errores=0;
		$msg = '<b>1.- Existen los directorios especificados en el fichero Directories.conf y no hay ningún fichero más en el directorio principal que el index.php:</b><br><br><table border="2">';
		
		#Recorre la lista de directorios.
		for($i=0;$i<count($dirConf);$i++){
			$actual = trim($dirConf[$i]);
			if($actual!=""){
				$msg.= '<tr><td>'.$actual.'</td>';
				#Comprueba si se trata de un directorio.
				if(is_dir($actual)){
					$msg.='<td><b>OK</b></td></tr>';
					$correctos++;
				}
				else{
					$msg.="<td><font color='red'><b>ERROR: No existe el directorio</b></font></td></tr>";
					$errores++;
				}
			}
			
		}
		
		#Recorre los ficheros de la raiz.
		for($j=0;$j<count($filesInicio);$j++){
			$actual = 'CodigoAExaminar/'.trim($filesInicio[$j]);
			#Comprueba si se trata de un fichero distinto al index.php.
			if(is_file($actual) && $actual!='CodigoAExaminar/index.php'){
				$msg.='<tr><td>'.$actual."<td><font color='red'><b>ERROR: Fichero no permitido</b></font></td></tr>";
				$errores++;
			}
			#Comprueba que se trate de index.php
			if(is_file($actual) && $actual=='CodigoAExaminar/index.php'){
				$msg.='<tr><td>'.$actual."<td><b>OK</b></td></tr>";
				$correctos++;
			}
		}
		$msg.=resumen($correctos,$errores);	
		return $msg;
	}
	
	
	#Realiza las comprobaciones del apartado 2.	
	function comprobacion2($filConf,$ficheros){
		$correctos=0;
		$errores=0;
		$msg = '<b>2.- Los ficheros tienen el nombre indicado en la especificación en el fichero Files.conf: </b><br><br><table border="2">';
		
		#Recorre el archivo Files.Conf.
		for($j=0;$j<count($filConf);$j++){
			if($filConf[$j]!=""){
				$msg.='<tr><td><b>'.$filConf[$j].'</b></td><td></td></tr>';
				$actualCond = '~'.trim($filConf[$j]).'$~';
				$actualCond = str_replace('%', '([A-Za-z]+)', $actualCond);
				$encontrado=false;
				#Recorre la lista de ficheros.
				for($i=0;$i<count($ficheros);$i++){
					$actualFil = $ficheros[$i];	
					#Comprueba si el fichero cumple las normas de Files.Conf.			
					if(preg_match($actualCond, $actualFil)==1){
						$encontrado=true;
						$msg.='<tr><td>'.$actualFil.'</td><td><b>OK</b></td></tr>';
						$correctos++;
						unset($ficheros[$i]); 
						$ficheros = array_values($ficheros);
						$i=$i-1;
					}
				}
				if(!$encontrado){
					$msg.="<td><font color='red'><b>Fichero no encontrado</b></font></td><td></td></tr>";
					$errores++;
				}
			}
		}
		if(count($ficheros)!=0){
			$msg.="<tr><td><font color='red'><b>FICHEROS INCORRECTOS</b></font></td><td></td></tr>";
			for($k=0;$k<count($ficheros);$k++){
				$msg.='<tr><td>'.$ficheros[$k]."</td><td><font color='red'><b>ERROR: Nombre no apto</b></font></td></tr>";
				$errores++;
			}
		}
		$msg.=resumen($correctos,$errores);	
		return $msg;
	}
	
	
	#Realiza las comprobaciones del apartado 3.	
	function comprobacion3($ficheros){
		$expr=array("~[Aa][Uu][Tt][Hh]?[Oo][Rr]~","~[Ff][Ee][Cc][Hh][Aa]~","~[Dd][Aa][Tt][Ee]~","~[Ff][Uu][Nn][Cc][Ii][Oo][Nn]~","~(([0-9]{2}/){2})([0-9]{2,4})~");
		$correctos=0;
		$errores=0;
		$msg = '<b>3.- Los ficheros del directorio CodigoAExaminar tiene todos al principio del fichero comentada su función, autor y fecha:</b><br><br><table border="2">';
		
		#Recorre la lista de ficheros.
		for($i=0;$i<count($ficheros);$i++){
			$msg.='<tr><td>'.$ficheros[$i].'</td>';
			$actualFil = lineas($ficheros[$i]);
			$resultado=0;
			#Recorre las lineas del fichero.
			for($j=0; $j<count($actualFil);$j++){
				$resultado += grep($expr,$actualFil[$j]);
			}
			#Comprueba si el resultado es correcto o incorrecto.
			if($resultado>1){
				$msg.='<td><b>OK</b></td></tr>';
				$correctos++;
			}
			#Comprueba si el resultado es correcto o incorrecto.
			else if($resultado==1){
				$msg.="<td><font color='red'><b>ERROR: La cabecera podría estar incompleta</b></font></td></tr>";
				$errores++;
			}
			else{
				$msg.="<td><font color='red'><b>ERROR: No tiene comentario de cabecera</b></font></td></tr>";
				$errores++;
			}
		}
		$msg.=resumen($correctos,$errores);	
		return $msg;
	}
	
	#Realiza las comprobaciones del apartado 4.	
	function comprobacion4($ficheros){
		$expr1=array("~function ([^_])([\w]+)([ ]?)(\()~");
		$expr2=array("~#~","~\*~","~//~");
		$correctos=0;
		$errores=0;
		$msg = '<b>4.- Las funciones y métodos en el código del directorio CodigoAExaminar tienen comentarios con una descripción antes de su comienzo: </b><br><br><table border="2">';
		
		#Recorre la lista de ficheros.
		for($i=0;$i<count($ficheros);$i++){
			$msg.='<tr><td>'.$ficheros[$i].'</td>';
			$actualFil = lineas($ficheros[$i]);
			$fallos=array();
			#Recorre las lineas del fichero.
			for($j=0; $j<count($actualFil);$j++){
				#Realiza una comprobación sobre una linea del fichero.
				if(grep($expr1,$actualFil[$j])){
					$resultado=false;
					$linea = lineaAnterior($actualFil,$j);
					#Realiza una comprobación sobre una linea del fichero.
					if(grep($expr2,$actualFil[$j]) || grep($expr2,$actualFil[$linea])){
						$resultado=true;
					}
					#Comprueba si el resultado es correcto o incorrecto.
					if($resultado){
						$correctos++;
					}
					else{
						$errores++;
						$fallos[]="<br>Linea ".($j+1)." - ".listarFuncion($actualFil[$j]);
					}
				}
			}
			#Comprueba si el resultado es correcto o incorrecto.
			if(count($fallos)==0){
				$msg.='<td><b>OK</b></td></tr>';
			}
			else{
				$msg.="<td><font color='red'><b>ERROR:";
				#Recorre y muestra la lista de errores.
				for($x=0;$x<count($fallos);$x++){
					$msg.= $fallos[$x].' ';
				}
				$msg.="</b></font></td></tr>";
			}
			unset($fallos);
		}
		$msg.=resumen($correctos,$errores);	
		return $msg;
	}
	
	
	#Realiza las comprobaciones del apartado 5.	
	function comprobacion5($ficheros){
		$expr1=array('~(\$)([^_])([\w]+)([ ]?)=~');
		$expr2=array("~#~","~\*~","~//~");
		$correctos=0;
		$errores=0;
		$msg = '<b>5.- En el código están todas las variables definidas antes de su uso y tienen un comentario antes o en la misma linea</b><br><br><table border="2">';
		$msg.= "<i><h6>(No se tienen en cuenta las variables índice para recorrer bucles.)</h6></i>";
		
		#Recorre la lista de ficheros.
		for($i=0;$i<count($ficheros);$i++){
			$msg.='<tr><td>'.$ficheros[$i].'</td>';
			$actualFil = lineas($ficheros[$i]);
			$fallos=array();
			$variables=array();
			#Recorre las lineas del fichero.
			for($j=0; $j<count($actualFil);$j++){
				#Realiza una comprobación sobre una linea del fichero.
				if(grep($expr1,$actualFil[$j])){
					$actualVar = trim(mostrarVariable($actualFil[$j]));
					#Comprueba que no se trate de una cadena vacia.
					if($actualVar!=""){
						#Comprueba si el resultado es correcto o incorrecto.
						if(!in_array($actualVar, $variables)){
							$variables[]=$actualVar;
							#Realiza una comprobación sobre una linea del fichero.
							if(grep($expr2,$actualFil[$j]) || grep($expr2,$actualFil[$j-1])){
								$correctos++;
							}
							else{
								$errores++;
								$fallos[]="<br>Linea ".($j+1)." - ".$actualVar;
							}
						}
					}	
				}
			}
			#Comprueba si el resultado es correcto o incorrecto.
			if(count($fallos)==0){
				$msg.='<td><b>OK</b></td></tr>';
			}
			else{
				$msg.="<td><font color='red'><b>ERROR:";
				#Recorre y muestra la lista de errores.
				for($x=0;$x<count($fallos);$x++){
					$msg.= $fallos[$x];
				}
				$msg.="</b></font></td></tr>";
			}
			unset($fallos);
			unset($variables);
		}
		$msg.=resumen($correctos,$errores);
		return $msg;
	}
	
	
	#Realiza las comprobaciones del apartado 6.	
	function comprobacion6($ficheros){
		$expr1=array("~if([ ]?)(\()~","~for([ ]?)(\()~","~foreach([ ]?)(\()~","~while([ ]?)(\()~","~switch([ ]?)(\()~","~else([ ]?)(\{)~","~case([ ]?)(:)~");
		$expr2=array("~#~","~\*~","~//~");
		$correctos=0;
		$errores=0;
		$msg = '<b>6.- En el código están comentadas todas las estructuras de control antes de uso o en la misma línea:</b> <br><br><table border="2">';
		
		#Recorre la lista de ficheros.
		for($i=0;$i<count($ficheros);$i++){
			$msg.='<tr><td>'.$ficheros[$i].'</td>';
			$actualFil = lineas($ficheros[$i]);
			$fallos=array();
			#Recorre las lineas del fichero.
			for($j=0; $j<count($actualFil);$j++){
				#Realiza una comprobación sobre una linea del fichero.
				if(grep($expr1,$actualFil[$j])){
					$linea = lineaAnterior($actualFil,$j);
					$estructura=listarEstructura($actualFil[$j]);
					#Comprueba si el resultado es correcto o incorrecto.
					if(grep($expr2,$actualFil[$j]) || grep($expr2,$actualFil[$linea])){
						$correctos++;
					}
					else{
						$errores++;
						$fallos[]="<br>Linea ".($j+1)." - ".$estructura;
					}
				}
			}
			#Comprueba si el resultado es correcto o incorrecto.
			if(count($fallos)==0){
				$msg.='<td><b>OK</b></td></tr>';
			}
			else{
				$msg.="<td><font color='red'><b>ERROR:";
				#Recorre y muestra la lista de errores.
				for($x=0;$x<count($fallos);$x++){
					$msg.= $fallos[$x].' ';
				}
				$msg.="</b></font></td></tr>";
			}
			unset($fallos);
		}
		$msg.=resumen($correctos,$errores);	
		return $msg;
	}
	
	
	#Realiza las comprobaciones del apartado 7.	
	function comprobacion7($ficheros){
		$expr1=array("~class ([\w])~");
		$expr2=array('~(\$)([\w]+)~',"~function([ ]?)~","~if([ ]?)(\()~","~for([ ]?)(\()~","~foreach([ ]?)(\()~","~while([ ]?)(\()~","~switch([ ]?)(\()~","~else([ ]?)(\{)~","~case([ ]?)(:)~","~echo~");
		$correctos=0;
		$errores=0;
		$msg = '<b>7.- Todos los ficheros dentro del directorio Model son definiciones de clases: </b><br><br><table border="2">';
		
		#Recorre la lista de ficheros.
		for($i=0;$i<count($ficheros);$i++){
			$msg.='<tr><td>'.$ficheros[$i].'</td>';
			$actualFil = lineas($ficheros[$i]);
			$resultado=true;
			$j=0;
			#Recorre las lineas del fichero
			while($j<count($actualFil) && $resultado){
				#Realiza una comprobación sobre una linea del fichero.
				if(grep($expr1,$actualFil[$j])){
					$j=cuentaCorchetes($actualFil,$j);
				}
				#Realiza una comprobación sobre una linea del fichero.
				if(grep($expr2,$actualFil[$j])){
					$resultado=false;
				}
				$j++;
			}
			#Comprueba si el resultado es correcto o incorrecto.
			if($resultado){
				$msg.='<td><b>OK</b></td></tr>';
				$correctos++;
			}
			else{
				$msg.="<td><font color='red'><b>ERROR: No es una clase</b></font></td></tr>";
				$errores++;
			}
		}
		$msg.=resumen($correctos,$errores);	
		return $msg;
	}
	
	
	#Realiza las comprobaciones del apartado 8.	
	function comprobacion8($ficheros){
		$expr1=array("~class ([\w])~");
		$expr2=array('~(\$)([\w]+)~',"~new([ ]?)~","~function([ ]?)~","~if([ ]?)(\()~","~for([ ]?)(\()~","~foreach([ ]?)(\()~","~while([ ]?)(\()~","~switch([ ]?)(\()~","~else([ ]?)(\{)~","~case([ ]?)(:)~","~echo~");
		$correctos=0;
		$errores=0;
		$msg = '<b>8.- Todos los ficheros dentro del directorio Controller son scripts: </b><br><br><table border="2">';
		
		#Recorre la lista de ficheros.
		for($i=0;$i<count($ficheros);$i++){
			$msg.='<tr><td>'.$ficheros[$i].'</td>';
			$actualFil = lineas($ficheros[$i]);
			$resultado=false;
			$j=0;
			#Recorre las lineas del fichero
			while($j<count($actualFil) && !$resultado){
				#Realiza una comprobación sobre una linea del fichero.
				if(grep($expr1,$actualFil[$j])){
					$j=cuentaCorchetes($actualFil,$j);
				}
				#Realiza una comprobación sobre una linea del fichero.
				if(grep($expr2,$actualFil[$j])){
					$resultado=true;
				}
				$j++;
			}
			#Comprueba si el resultado es correcto o incorrecto.
			if($resultado){
				$msg.='<td><b>OK</b></td></tr>';
				$correctos++;
			}
			else{
				$msg.="<td><font color='red'><b>ERROR: No es un script php</b></font></td></tr>";
				$errores++;
			}
		}
		$msg.=resumen($correctos,$errores);	
		return $msg;
	}
	
	
	
	#Realiza las comprobaciones del apartado 9.	
	function comprobacion9($ficheros){
		$expr1=array("~class ([\w])~");
		$expr2=array('~(\$)([\w]+)~',"~function([ ]?)~","~if([ ]?)(\()~","~for([ ]?)(\()~","~foreach([ ]?)(\()~","~while([ ]?)(\()~","~switch([ ]?)(\()~","~else([ ]?)(\{)~","~case([ ]?)(:)~","~echo~");
		$correctos=0;
		$errores=0;
		$msg = '<b>9.- Todos los ficheros dentro del directorio View son definiciones de clases: </b><br><br><table border="2">';
		
		#Recorre la lista de ficheros.
		for($i=0;$i<count($ficheros);$i++){
			$msg.='<tr><td>'.$ficheros[$i].'</td>';
			$actualFil = lineas($ficheros[$i]);
			$resultado=true;
			$j=0;
			#Recorre las lineas del fichero
			while($j<count($actualFil) && $resultado){
				#Realiza una comprobación sobre una linea del fichero.
				if(grep($expr1,$actualFil[$j])){
					$j=cuentaCorchetes($actualFil,$j);
				}
				#Realiza una comprobación sobre una linea del fichero.
				if(grep($expr2,$actualFil[$j])){
					$resultado=false;
				}
				$j++;
			}
			#Comprueba si el resultado es correcto o incorrecto.
			if($resultado){
				$msg.='<td><b>OK</b></td></tr>';
				$correctos++;
			}
			else{
				$msg.="<td><font color='red'><b>ERROR: No es una clase</b></font></td></tr>";
				$errores++;
			}
		}
		$msg.=resumen($correctos,$errores);	
		return $msg;
	}
	
	
	
	#Elimina las lineas en blanco de un fichero.
	function lineas($fichero){
		$actualFil = explode("\n" , file_get_contents($fichero));
		#Recorre las lineas del fichero.
		for($i=0;$i<count($actualFil);$i++){
			$actualFil[$i]=trim($actualFil[$i]);
		}
		return $actualFil;
	}
	
	#Comprueba una expresión regular en una linea del fichero.
	function grep($expr, $linea){
		#Recorre la lista de expresiones regulares.
		for($i=0;$i<count($expr);$i++){
			#Comprueba una expresión regular en la linea.
			if(preg_match($expr[$i], $linea)==1){
				return true;
			}
		}
		return false;
	}
	
	#Retrocede lineas de fichero hasta encontrar una linea con contenido.
	function lineaAnterior($fichero,$linea){
		$linea--;
		#Retrocede una linea y comprueba.
		while($fichero[$linea]=="" && $linea>0){
			$linea--;
		}
		return $linea;
	}
	
	#Extrae de una linea el nombre de la funcion.
	function listarFuncion($linea){
		$inicio="function";
		$fin="("; 
		$total= (strlen($linea)-stripos($linea,$fin)); 
		$final= substr ($linea,strpos($linea,$inicio),-$total); 
		return $final;
	}
	
	#Extrae de una linea el nombre de una estructura.
	function listarEstructura($linea){
		#Realiza una comprobación sobre una linea del fichero.
		if(grep(array("~if([ ]?)(\()~"),$linea)){
			return "if";
		}
		#Realiza una comprobación sobre una linea del fichero.
		if(grep(array("~for([ ]?)(\()~"),$linea)){
			return "for";
		}
		#Realiza una comprobación sobre una linea del fichero.
		if(grep(array("~foreach([ ]?)(\()~"),$linea)){
			return "foreach";
		}
		#Realiza una comprobación sobre una linea del fichero.
		if(grep(array("~while([ ]?)(\()~"),$linea)){
			return "while";
		}
		#Realiza una comprobación sobre una linea del fichero.
		if(grep(array("~switch([ ]?)(\()~"),$linea)){
			return "switch";
		}
		#Realiza una comprobación sobre una linea del fichero.
		if(grep(array("~else([ ]?)(\{)~"),$linea)){
			return "else";
		}
		#Realiza una comprobación sobre una linea del fichero.
		if(grep(array("~case([ ]?)(:)~"),$linea)){
			return "case";
		}
		return "";
	}
	
	#Extrae de una linea el nombre de la variable.
	function mostrarVariable($linea){
		$inicio="\$";
		$fin="="; 
		$total= (strlen($linea)-stripos($linea,$fin)); 
		$final= substr ($linea,strpos($linea,$inicio),-$total); 
		return $final;
	}
	
	
	function cuentaCorchetes($fichero,$linea){
		$abierto=array("~{~");
		$dobleAbierto=array("~{{~");
		$cerrado=array("~}~");
		$dobleCerrado=array("~}}~");
		$open=0;
		$close=0;
		#Comprueba cuando se abre la clase.
		if(grep($abierto,$fichero[$linea])){
			$open++;
			$linea++;
		}
		#Comprueba cuando se abre la clase.
		else if(grep($abierto,$fichero[$linea+1])){
			$open++;
			$linea=$linea+2;
		}
		#Avanza por la clase contando corchetes.
		while($linea<count($fichero) && $open!=$close){
			#Comprueba si se abre un corchete.
			if(grep($dobleAbierto,$fichero[$linea])){
				$open=$open+2;
			}
			else if(grep($abierto,$fichero[$linea])){
				$open++;
			}
			#Comprueba si se cierra un corchete.
			if(grep($dobleCerrado,$fichero[$linea])){
				$close=$close+2;
			}
			else if(grep($cerrado,$fichero[$linea])){
				$close++;
			}
			$linea++;
		}
		return $linea;
	}
	
	
	#Mensaje resumen del apartado generado dinamicamente.
	function resumen($correctos, $errores){
		$msg;
		#Comprueba si el resultado es correcto o incorrecto.
		if($errores>0){
			$msg='</table><br><b>RESUMEN</b>: '.($correctos+$errores)." Elementos Analizados | <font color='red'><b>Numero de errores: ".$errores."</b></font><br><br><br>";
		}
		else{
			$msg='</table><br><b>RESUMEN</b>: '.($correctos+$errores)." Elementos Analizados | Numero de errores: ".$errores."<br><br><br>";
		}
		return $msg;
	}

?>